---
title: Libraries
---

These are libraries to make interaction with signald easier.

# Generated

these libraries make use of signald's protocol documentation to generate their core. This ensures they will
support a full range of signald functionality, and naming will be consistant with signald of the documentation:

* [signald-go](https://gitlab.com/signald/signald-go) (golang)
* [pysignald-async](https://gitlab.com/nicocool84/pysignald-async) (python)

# Other

* [pysignald](https://pypi.org/project/pysignald/) (python)
* [Semaphore](https://github.com/lwesterhof/semaphore) (python) - a simple (rule-based) bot library