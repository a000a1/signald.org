The docker image can be pulled from:
* docker hub: `finn/signald`
* GitLab's container registry: `registry.gitlab.com/signald/signald`

The container will create a socket at `/signald/signald.sock` within the container. To access the socket from outside the container, mount a directory to `/signald`:

```bash
docker run -v $(pwd)/run:/signald finn/signald
```

## Additional tooling

Consider installing [signaldctl](/signaldctl/).