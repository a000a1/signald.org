---
title: signald
---
*unofficial daemon for interacting with Signal*

{{<hint info>}}
This site is a work in progress. If you have questions, [file an issue](https://gitlab.com/signald/signald/-/issues/new) or ask on [IRC](/articles/IRC/).
{{</hint>}}

[![source](/badges/stars.svg)](https://gitlab.com/signald/signald)
[![docker](/badges/docker.svg)](https://hub.docker.com/r/finn/signald)
[![GPL](/badges/license.svg)](https://gitlab.com/signald/signald/-/blob/main/LICENSE)


Signal does not offer any sort of official API. Their server software is [open source](https://github.com/SignalApp/Signal-Server) and anyone can
figure out the REST endpoints, but formatting the payloads turns out to be a little trickier. Signal, unlike traditional messaging applications,
puts a number requirements around encryption and account data storage on the client software. signald aims to handle all of the Signal-specific
requirements, exposing an API that sends and receives unencrypted messages and can be queried for account data (contacts, groups, etc).

signald does not come with a user friendly frontend. Users should an existing [client](/articles/clients/) to use signald with.

Developers wanting to learn how to write code to interact with signald, check out [getting Started](/articles/getting-started/) page.